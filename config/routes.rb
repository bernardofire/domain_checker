Rails.application.routes.draw do
  devise_for :users
  root to: 'verifications#new'

  resources :verifications, only: [:new, :create, :update]
  resources :domains
end
