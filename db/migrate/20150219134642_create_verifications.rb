class CreateVerifications < ActiveRecord::Migration
  def change
    create_table :verifications do |t|
      t.string :name
      t.integer :status

      t.timestamps null: false
    end
  end
end
