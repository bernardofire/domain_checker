class VerificationsController < ApplicationController
  before_action :set_verification, only: [:update]

  def new
    @verification = Verification.new
  end

  def create
    @verification = Verification.create(verification_params)

    render :new
  end

  def update
    @verification.update_attributes(verification_params)

    render :new
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_verification
      @verification = Verification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def verification_params
      params.require(:verification).permit(:name)
    end
end
