class DomainsController < ApplicationController
  before_action :set_domain, only: [:destroy]
  before_action :authenticate_user!

  def index
    @domains = current_user.domain.all
  end

  def create
    @domain = Domain.new(domain_params)
    @domain.user = current_user
    @domain.save
    redirect_to new_verification_path
  end

  def destroy
    @domain.destroy
    redirect_to domains_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_domain
      @domain = Domain.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def domain_params
      { name: params.require(:domain_name) }
    end
end
