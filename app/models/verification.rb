class Verification < ActiveRecord::Base
  before_save :set_status

  validates :name, presence: true

  enum status: [:available, :unavailable]

  private

  def set_status
    if !Whois.available?(name) || Domain.where(name: self.name).any?
      self.status = 'unavailable'
    else
      self.status = 'available'
    end
  end
end
