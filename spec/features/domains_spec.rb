require 'rails_helper'

describe Domain do
  describe 'User control' do
    scenario 'redirects to login page if try to access a domain path' do
      visit domains_path

      expect(current_path).to be_eql(new_user_session_path)
    end
  end

  describe '#index' do
    scenario 'show reserved domains' do
      create_and_login_user
      domain = FactoryGirl.create(:domain, user: User.last)
      visit domains_path

      expect(page).to have_content(domain.name)
    end

    scenario 'show message when there is no domain' do
      create_and_login_user
      visit domains_path

      expect(page).to have_content('There is no reserved domain')
    end

    scenario 'destroy reservation' do
      create_and_login_user
      domain = FactoryGirl.create(:domain, user: User.last)
      visit domains_path
      click_link 'Destroy'

      expect(Domain.all).to be_empty
    end
  end
end
