require 'rails_helper'

describe 'Domain Verification' do
  describe 'Verify' do
    scenario 'when the domain is available' do
      mock_whois_available
      verify_google_domain
      verification = Verification.last

      expect(verification.name).to be_eql('www.google.com')
      expect(verification.available?).to be_eql(true)
      expect(page).to have_content("The domain 'www.google.com' is available")
    end

    scenario 'when the domain is unavailable' do
      visit new_verification_path
      verify_google_domain
      verification = Verification.last


      expect(verification.name).to be_eql('www.google.com')
      expect(verification.unavailable?).to be_eql(true)
      expect(page).to have_content("The domain 'www.google.com' is unavailable =/")
    end

    describe 'Reserve button' do
      scenario 'Only appears when user is logged in' do
        mock_whois_available
        verify_google_domain

        expect(page).to_not have_button('Reserve!')
      end

      scenario 'Appears when the domain is available' do
        mock_whois_available
        create_and_login_user
        verify_google_domain

        expect(page).to have_button('Reserve!')
      end

      scenario 'Does not appear when the domain is unavailable' do
        create_and_login_user
        verify_google_domain

        expect(page).to_not have_button('Reserve!')
      end
    end
  end
end
