def verify_google_domain
  visit new_verification_path
  domain_name = 'www.google.com'
  fill_in 'Name', with: domain_name
  click_button 'Verify'
end

def mock_whois_available
  allow(Whois).to receive(:available?) { true }
end
