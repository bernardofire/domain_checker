require 'rails_helper'

RSpec.describe Verification, type: :model do
  context 'Validations' do
    context 'Attributes' do
      it { should validate_presence_of(:name) }
      it { should_not validate_presence_of(:status) }
    end
  end

  it 'Attributes' do
    verification = FactoryGirl.create(:verification)

    expect(verification).to respond_to(:name)
    expect(verification).to respond_to(:status)
  end

  context 'Verify and set status before creation' do
    context 'unavailable' do
      it 'with reserved domain' do
        mock_whois_available
        FactoryGirl.create(:domain, name: 'google.com', user: FactoryGirl.create(:user))
        verification = FactoryGirl.create(:verification, name: 'google.com', status: nil)

        expect(verification.unavailable?).to be_eql(true)
      end

      it 'with unreserved domain' do
        verification = FactoryGirl.create(:verification, name: 'google.com', status: nil)

        expect(verification.unavailable?).to be_eql(true)
      end
    end

    it 'available' do
      mock_whois_available

      verification = FactoryGirl.create(:verification, name: 'google.com', status: nil)

      expect(verification.available?).to be_eql(true)
    end
  end
end
