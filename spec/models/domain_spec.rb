require 'rails_helper'

RSpec.describe Domain, type: :model do
  context 'Relationships' do
    it { should belong_to(:user) }
  end

  context 'Validations' do
    context 'Relationships' do
      it { should validate_presence_of(:user) }
    end

    context 'Attributes' do
      it { should validate_presence_of(:name) }
    end
  end

  it 'Attributes' do
    verification = FactoryGirl.create(:domain)

    expect(verification).to respond_to(:name)
  end
end
