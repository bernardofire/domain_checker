require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Relationships' do
    it { should have_many(:domain) }
  end
end
